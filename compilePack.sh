#!/bin/sh
sudo rm -rf temp/* output/* Jackapack.zip
mkdir -p temp
mkdir -p output
cd temp

# Compliance
git clone git@github.com:Compliance-Resource-Pack/Compliance-Java-32x.git --depth 1 --branch Jappa-1.18 --single-branch Compliance
cp -r --remove-destination Compliance/* ../output
# Compliance Addon Pack (to be copied in order later on)
git clone git@github.com:Compliance-Addons/Addons.git --depth 1 --branch master --single-branch ComplianceAddons

# Fences+
fencesplus=$(curl https://curse.nikky.moe/api/addon/416589 | jq --raw-output '.latestFiles[0].downloadUrl')
wget "$fencesplus" -O fences+.zip
unzip -d Fences+ fences+.zip
cp -r --remove-destination Fences+/assets/* ../output/assets

# Custom Paintings
# Sourced from the Addons Pack
unzip -d CustomPaintings 'ComplianceAddons/32x/Custom Paintings/Custom Paintings 1.16.2.zip'
cp -r --remove-destination CustomPaintings/assets/* ../output/assets

# Extended Soundtrack
# TODO: was deleted due to copyright issues, need to find or create replacement
#extsound=$(curl https://curse.nikky.moe/api/addon/405770 | jq --raw-output '.latestFiles[0].downloadUrl')
#wget "$extsound" -O extsound.zip
#unzip -d ExtendedSoundtrack extsound.zip
#cp -r --remove-destination ExtendedSoundtrack/assets/* ../output/assets

# Extra Panoramas
extrapano=$(curl https://curse.nikky.moe/api/addon/432539 | jq --raw-output '.latestFiles[0].downloadUrl')
wget "$extrapano" -O extrapano.zip
unzip -d ExtraPanoramas extrapano.zip
cp -r --remove-destination ExtraPanoramas/assets/* ../output/assets

# Default 3D
#wget https://beta.know2good.com/dl/09.09.20/%C2%A7lDefault%C2%A7r..%C2%A7l3D%C2%A7r..High%C2%A70%C2%A7o.zip -O default3d.zip
#unzip -d Default3D default3d.zip
#cp -r --remove-destination Default3D/assets/* ../output/assets

# Compliance 3D
git clone git@github.com:Faithful3D/Java-32x.git --depth 1 --branch master --single-branch Compliance3D
cp -r --remove-destination Compliance3D/assets/* ../output/assets

# Dark UI
darkui=$(curl https://curse.nikky.moe/api/addon/377388 | jq --raw-output '.latestFiles[0].downloadUrl')
wget "$darkui" -O darkui.zip
unzip -d DarkUI darkui.zip
cp -r --remove-destination DarkUI/assets/* ../output/assets

# Dale Shader
git clone git@github.com:voormann/dale-shader.git --depth 1 Dale
cp -r --remove-destination Dale/Dale/assets/* ../output/assets

# Custom Tweaks
cp -r --remove-destination ../custom/Music/assets/* ../output/assets				# More Music
cp -r --remove-destination ../custom/InvisibleItemFrames/assets/* ../output/assets  # Invisible Item Frames
cp -r --remove-destination ../custom/NoJavaEdition/assets/* ../output/assets        # No Java Edition Banner
cp -r --remove-destination ../custom/NoPumpkinBlur/assets/* ../output/assets        # No Pumpkin Blur
#cp -r --remove-destination ../custom/3D/assets/* ../output/assets					# 3D
rm ../output/assets/minecraft/texts/splashes.txt                                    # Don't Overwrite Splash Screens
rm ../output/assets/minecraft/textures/gui/title/background/panorama_*.png          # Don't Overwrite Background Panorama

# Final Packaging
cp ../pack.* ../output
cd ../output
zip -r ../Jackapack.zip *